# CRUD en JS

Aplicacion CRUD muy simple escrita en Javascript, con fines didácticos.
Se implementa utilizando un array, como asi tambien mediante el uso de una base de datos en mysql.
Requiere el uso de node y npm

## Descarga

`git clone https://gitlab.com/luirro777/crudjsv2`

## Uso

### Sin base de datos

Con esta opción, los datos se almacenarán en un array llamado `users`, por lo cual al cerrar la aplicación, toda la info se perderá.

`cd nodb`

`npm install`

`node app.js`


### Con base de datos

Previamente, se deberá instalar mysql y crear una base de datos. La misma deberá tener una tabla llamada `users`, con 3 campos: id, name, email.
Modificar los datos de conexion desde el archivo app.js

`cd mysql`

`npm install`

`node app.js`



