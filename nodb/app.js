const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true }));
// Configura la entrega de archivos estáticos desde la carpeta 'public'
app.use(express.static('public'));

let users = [];

app.get('/', (req, res) => {
    res.render('index', { users });
});

app.get('/add', (req, res) => {
    res.render('add');
});

app.post('/add', (req, res) => {
    const { name, email } = req.body;
    users.push({ name, email });
    res.redirect('/');
});

app.get('/edit/:id', (req, res) => {
    const id = req.params.id;
    const user = users[id];
    res.render('edit', { id, user });
});

app.post('/edit/:id', (req, res) => {
    const id = req.params.id;
    const { name, email } = req.body;
    users[id] = { name, email };
    res.redirect('/');
});

app.get('/delete/:id', (req, res) => {
    const id = req.params.id;
    users.splice(id, 1);
    res.redirect('/');
});

app.listen(port, () => {
    console.log(`Servidor en http://localhost:${port}`);
});
