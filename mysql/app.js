const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
const mysql = require('mysql');

const dbConfig = {
    host: '127.0.0.1',
    user: 'crudjs',
    password: '1234',
    database: 'crudjs'
};

const connection = mysql.createConnection(dbConfig);

connection.connect((error) => {
    if (error) {
        console.error('Error al conectar a la base de datos:', error);
    } else {
        console.log('Conexión exitosa a la base de datos');
    }
});

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true }));
// Configura la entrega de archivos estáticos desde la carpeta 'public'
app.use(express.static('public'));



// Obtener la lista de usuarios desde la base de datos
app.get('/', (req, res) => {
    const query = 'SELECT * FROM users';
    connection.query(query, (error, results) => {
        if (error) throw error;
        res.render('index', { users: results });
    });
});

app.get('/add', (req, res) => {
    res.render('add');
});

// Agregar un nuevo usuario a la base de datos
app.post('/add', (req, res) => {
    const { name, email } = req.body;
    const query = 'INSERT INTO users (name, email) VALUES (?, ?)';
    connection.query(query, [name, email], (error, result) => {
        if (error) throw error;
        res.redirect('/');
    });
});

// Mostrar el formulario de edición
app.get('/edit/:id', (req, res) => {
    const id = req.params.id;
    const query = 'SELECT * FROM users WHERE id = ?';
    connection.query(query, [id], (error, results) => {
        if (error) throw error;
        res.render('edit', { user: results[0] });
    });
});

// Actualizar un usuario en la base de datos
app.post('/edit/:id', (req, res) => {
    const id = req.params.id;
    const { name, email } = req.body;
    const query = 'UPDATE users SET name = ?, email = ? WHERE id = ?';
    connection.query(query, [name, email, id], (error, result) => {
        if (error) throw error;
        res.redirect('/');
    });
});

// Borrar un usuario de la base de datos
app.get('/delete/:id', (req, res) => {
    const id = req.params.id;
    const query = 'DELETE FROM users WHERE id = ?';
    connection.query(query, [id], (error, result) => {
        if (error) throw error;
        res.redirect('/');
    });
});

process.on('SIGINT', () => {
    connection.end();
    console.log('Conexión a la base de datos cerrada');
    process.exit();
});

app.listen(port, () => {
    console.log(`Servidor en http://localhost:${port}`);
});
